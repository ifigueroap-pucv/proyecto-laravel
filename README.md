# Proyecto Laravel v5.4


## Instrucciones

1. Instalar XAMPP, para así tener PHP y MySQL.

1. Instalar [Composer](https://getcomposer.org). En la instalación se buscará el ejecutable de PHP que en general está en `C:\xampp\php\php.exe`. Probablemente sea detectado por defecto.

1. Utilizando XAMPP u otra herramienta, debe crear una base de datos MySQL vacía, que se llame `proyecto`. Como usuario ponga `root` y que no tenga password.

1. Descargar [archivo comprimido de este repositorio](https://bitbucket.org/ifigueroap-pucv/proyecto-laravel/downloads/), y descomprimirlo en la carpeta `proyecto-laravel`. Esta carpeta puede estar en cualquier lugar de su computador, no necesariamente dentro de XAMPP.

1. Abrir `cmd` y dirigirse a la carpeta `$DocumentRoot/proyecto-lavarel/blog`.

1. Para crear la base de datos con los modelos/entidades del proyecto ejecute el comando: `php artisan migrate`.

1. Para ejecutar un servidor local para probar su aplicación ejecute el comando: `php artisan serve`. La aplicación estará disponible en `http://localhost:8000`.

## Configuración Login Estándar Laravel

Este proyecto ya viene configurado con el sistema de autentificación estándar de Laravel. Los usuarios pueden registrarse, iniciar sesión, y recuperar sus contraseñas por correo en caso de problemas. Es necesario configurar el correo que enviará las confirmaciones a los usuarios. Esto se hace en el archivo `.env`, el cual viene preconfigurado con lo siguiente:

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=pruebaweb.2017.2@gmail.com
MAIL_PASSWORD=zgqtdyiathawnzli
MAIL_FROM=pruebaweb.2017.2@gmail.com
MAIL_NAME=Web
```

Su proyecto debe reemplazar esta información con una cuenta dedicada.

## Configuración Login con Redes Sociales

Este proyecto ya viene configurado con el plugin [Socialite](https://github.com/laravel/socialite) que permite iniciar sesión con cuentas de redes sociales, tales como Google, Facebook y Twitter.

### Crear Aplicación en Redes Sociales

Si bien el proyecto viene listo para iniciar sesión con redes sociales, cada proyecto debe crear su propia aplicación, en cada uno de los proveedores que desea utilizar. Luego, debe configurar el proyecto con los datos asociados a cada servicio. Esto se hace en el archivo `app/config/services.php`. Por defecto el proyecto está configurado para Google, Facebook y Twitter:

```
'google' => [
	'client_id' => '244470518967-8bpq3a65eut3emd1v4gicja0j7odc85e.apps.googleusercontent.com',
	'client_secret' => 'vVLjJiZb_gBOI7_SQ-lyglUz',
	'redirect' => 'http://localhost:8000/login/google/callback',
],

'twitter' => [
	'client_id' => 'XYZfAjh0LPf3WfTyBoBD3CQII',
	'client_secret' => 'oJ55pGXWFkGPi7EQKJ6jRqCYB01EPTnU0Hj1P0U03cJTbeiQLz',
	'redirect' => 'http://localhost:8000/login/twitter/callback',
],

'facebook' => [
	'client_id' => '2013859102166944',
	'client_secret' => 'df7a816797ce02841da52c64dab33b1b',
	'redirect' => 'http://localhost:8000/login/facebook/callback',
],
```

Cuando usted cree su(s) aplicación(es) en estos servicios, debe cambiar los valores de `client_id` y `client_secret` respectivamente. 

## Personalizar el Esqueleto para su Propio Proyecto

En resumen, para personalizar el esqueleto de acuerdo a su proyecto debe realizar por lo menos lo siguiente:

1. Pedir al profesor la creación de una base de datos en el servidor `beta.inf.ucv.cl` y modificar el archivo `.env` con las credenciales de acceso. Luego debe realizar las migraciones (`php artisan migrate`) y trabajar con esa base de datos.

1. Crear una cuenta de correo Gmail dedicada para su proyecto, y cambiar la configuración en el archivo `.env`.

1. Crear aplicaciones en redes sociales, para utilizar el mecanismo de login social. Actualizar la configuración en `app/config/services.php`.