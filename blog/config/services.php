<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '244470518967-8bpq3a65eut3emd1v4gicja0j7odc85e.apps.googleusercontent.com',
        'client_secret' => 'vVLjJiZb_gBOI7_SQ-lyglUz',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],
 
    'twitter' => [
        'client_id' => 'XYZfAjh0LPf3WfTyBoBD3CQII',
        'client_secret' => 'oJ55pGXWFkGPi7EQKJ6jRqCYB01EPTnU0Hj1P0U03cJTbeiQLz',
        'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],

    'facebook' => [
        'client_id' => '2013859102166944',
        'client_secret' => 'df7a816797ce02841da52c64dab33b1b',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
];
